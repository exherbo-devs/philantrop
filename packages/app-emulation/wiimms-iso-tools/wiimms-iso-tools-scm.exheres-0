# Copyright 2009-2015 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SCM_REPOSITORY="http://opensvn.wiimm.de/wii/"
SCM_SUBPATH="wiimms-iso-tools"

require scm-svn

SUMMARY="A command-line WBFS toolset"
DESCRIPTION="
A WBFS-formatted harddisk (partition) allows you to store backups of your original
Wii game discs on it and play your games from it. This is faster than from the
original DVD and, obviously, prevents wearing it down. This toolset allows you
to use WBFS.
"
HOMEPAGE="http://wit.wiimm.de"
DOWNLOADS=""


LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""
DEPENDENCIES="
    build+run:
        !app-emulation/wiimms-wbfs-tool [[
            description = [ Package was renamed ]
            resolution = uninstall-blocked-after
        ]]
"

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/${PN}_Makefile.patch )

# make test does not run tests but adds crap to the tools
RESTRICT="test"

src_prepare() {
    default

    export PRE=$(exhost --tool-prefix)

    edo sed -i -e "s:gcc:$(exhost --tool-prefix)&:" setup.sh
    edo sed -i -e "/INSTALL_PATH/s:/usr:&/$(exhost --target):" setup.sh
    edo sed -i -e "/SHARE_PATH/s:\$(INSTALL_PATH):${IMAGE}/usr:" Makefile
}

src_compile() {
    # Make it respect the user's CFLAGS.
    export XFLAGS="${CFLAGS}"
    # Fails to compile using emake due to the weird Makefile.
    edo make flags all doc
}

src_install() {
    # Upstreams...
    edo chmod +x "${WORK}"/setup/install.sh
    default

    # The script is supposed to be run during installation but it fetches stuff
    # from the internet. We install it without ${IMAGE} in it so that it can be
    # run by the user later on.
    edo sed -i -e "s:\(BASE_PATH=\"\)${IMAGE}\(.*\):\1\2:" setup/load-titles.sh

    # Install the docs and some helper scripts.
    dodoc doc/*
    dodoc -r scripts
    docinto scripts
    dodoc setup/load-titles.sh
}

