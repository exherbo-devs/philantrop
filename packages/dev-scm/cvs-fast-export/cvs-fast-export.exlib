# Copyright 2014-2019 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

export_exlib_phases src_prepare

SUMMARY="Export an RCS or CVS history as a fast-import stream"
DESCRIPTION="
This program analyzes a collection of RCS files in a CVS repository (or outside
of one) and, when possible, emits an equivalent history in the form of a fast-import
stream. Not all possible histories can be rendered this way; the program tries
to emit useful warnings when it can't. The program can also produce a visualization
of the resulting commit DAG in the DOT format handled by the graphviz suite.
The package also includes cvssync, a tool for mirroring masters from remote CVS
hosts.
"
HOMEPAGE="http://www.catb.org/~esr/${PN}"
DOWNLOADS="${HOMEPAGE}/${PNV}.tar.gz"

UPSTREAM_RELEASE_NOTES="https://gitlab.com/esr/${PN}/blob/master/NEWS [[ lang = en ]]"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        app-doc/asciidoc
        sys-devel/bison
        sys-devel/flex
    run:
        dev-lang/python:*
    test:
        dev-scm/cvs
"

DEFAULT_SRC_COMPILE_PARAMS=( -j1 )

DEFAULT_SRC_INSTALL_PARAMS=(
    prefix=/usr
    mandir="${IMAGE}"/usr/share/man
)

cvs-fast-export_src_prepare() {
    default

    # Respect CFLAGS
    edo sed -i -e "s:\(CFLAGS\)=:\1+=:" Makefile
    edo sed -i -e "/CFLAGS += -O3/d" Makefile

    # Fix bindir
    edo sed -i -e "s:/bin:/$(exhost --target)/&:" Makefile
}

