# Copyright 2010-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ release=v${PV} suffix=tar.bz2 ] \
    pam

SUMMARY="libcg is a library that abstracts the control group file system in Linux"
DESCRIPTION="
libcgroup aims to provide programmers easily usable APIs to use the control group
file system. It should satisfy the following requirements:
- Provide a programmable interface for cgroups
- Provide persistent configuration across reboots
- Provide a programmable interface for manipulating configurations
"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        sys-libs/pam
"

# The tests since 0.38 want to mess directly with /sys *and* expect libcgroup to
# be already installed. Last checked: 0.41
RESTRICT="test"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-cgred-socket=/run/cgred.socket
    --enable-daemon
    --enable-opaque-hierarchy=name=systemd
    --enable-pam
    --enable-pam-module-dir=$(getpam_mod_dir)
    --enable-shared
    --enable-tools
    --disable-bindings
    --disable-initscript-install
    --disable-static
)

