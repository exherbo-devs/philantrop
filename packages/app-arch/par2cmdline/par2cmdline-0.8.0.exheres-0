# Copyright 2009-2018 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=Parchive tag=v0.7.4 ] autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="A command line implementation of the PAR v2.0 specification"
DESCRIPTION="
This specification is used for parity checking and repair of a file set. If the
files in the recovery set ever get damaged (e.g. when they are transmitted or
stored on a faulty disk) the client can read the damaged input files, read the
(possibly damaged) PAR files, and regenerate the original input files. Of course,
not all damages can be repaired, but many can.
For years now, PAR 2.0 has been the de-facto standard for verifying and repairing
multi-part files downloaded from Usenet.

This version has multi-thread support via OpenMP.
"

HOMEPAGE="
    https://github.com/Parchive/par2cmdline
"
#    https://github.com/jkansanen/par2cmdline-mt

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        sys-libs/libgomp:=
"

