# Copyright 2009-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="${PN} is used to monitor the progress of data through a pipe"
DESCRIPTION="
${PN} - Pipe Viewer - is a terminal-based tool for monitoring the progress of data
through a pipeline. It can be inserted into any normal pipeline between two processes
to give a visual indication of how quickly data is passing through, how long it
has taken, how near to completion it is, and an estimate of how long it will be
until completion.
"
BASE_URI="https://www.ivarch.com/programs"
HOMEPAGE="${BASE_URI}/${PN}.shtml"
DOWNLOADS="${BASE_URI}/sources/${PNV}.tar.gz"

REMOTE_IDS="freecode:${PN} sourceforge:pipeviewer"

UPSTREAM_CHANGELOG="http://code.google.com/p/pipeviewer/source/browse/trunk/doc/NEWS"
UPSTREAM_DOCUMENTATION="${BASE_URI}/programs/quickref/${PN}.shtml [[ lang = en ]]"

LICENCES="Artistic-2.0"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="debug"

DEPENDENCIES="
    build:
        sys-devel/gettext
"

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( "debug debugging" )
DEFAULT_SRC_CONFIGURE_PARAMS=(
    --hates=docdir
    --enable-nls
)
DEFAULT_SRC_INSTALL_EXTRA_PREFIXES=( doc/ )
DEFAULT_SRC_INSTALL_EXCLUDE=( '*/release-checklist' )

